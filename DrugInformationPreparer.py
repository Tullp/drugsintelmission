
import re
from datetime import datetime, timedelta


class DrugInformationPreparer:

	def __init__(self, registry_api_data: dict):
		self.registry_api_data = registry_api_data

	def get_info(self) -> dict[str, str]:
		return {
			"Trade name": self.__get_name(),
			"Active substances": self.__get_active_substances(),
			"Strength": self.__get_strength(),
			"Route of Administration": self.__get_route_of_administration(),
			"Form": self.__get_form(),
			"Registration number": self.__get_registration_number(),
			"MAH": self.__get_mah(),
			"Manufacturer": self.__get_manufacturer(),
			"Approval date": self.__get_approval_date(),
			"Application date": self.__get_application_date(),
			"URL": self.__get_url()
		}

	def __get_name(self) -> str:
		return self.registry_api_data["dragEnName"]

	def __get_active_substances(self) -> str:
		titles = sorted(info["ingredientsDesc"] for info in self.registry_api_data["activeMetirals"])
		return ", ".join(re.sub("\\s+", " ", title).strip() for title in titles)

	def __get_strength(self) -> str:
		dosages = sorted(info["dosage"] for info in self.registry_api_data["activeMetirals"])
		return ", ".join(re.sub("\\s+", " ", dosage).strip() for dosage in dosages)

	def __get_route_of_administration(self) -> str:
		return self.registry_api_data["usageFormEng"]

	def __get_form(self) -> str:
		return self.registry_api_data["dosageFormEng"]

	def __get_registration_number(self) -> str:
		return self.registry_api_data["dragRegNum"]

	def __get_mah(self) -> str:
		return self.registry_api_data["regOwnerName"]

	def __get_manufacturer(self) -> str:
		return self.registry_api_data["regManufactureName"]

	def __get_approval_date(self) -> str:
		timestamp = self.registry_api_data["regDate"] / 1000
		return (datetime(1970, 1, 1, 2) + timedelta(seconds=timestamp)).strftime("%Y-%m-%d")

	def __get_application_date(self) -> str:
		timestamp = self.registry_api_data["applicationDate"] / 1000
		return (datetime(1970, 1, 1, 2) + timedelta(seconds=timestamp)).strftime("%Y-%m-%d")

	def __get_url(self) -> str:
		return f"https://data.health.gov.il/drugs/index.html#!/medDetails/{self.__get_registration_number()}"
