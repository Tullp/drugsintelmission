
import json
import requests

from DrugInformationPreparer import DrugInformationPreparer
from DrugsSearcher import DrugsSearcher


def get_last_page() -> int:
	with open("last_page", "r") as f:
		return int(f.read())


def set_last_page(last_page: int):
	with open("last_page", "w") as f:
		f.write(str(last_page))


def update_id_list(additional_id_list: list[str]):
	with open("id_list.json", "r") as f:
		id_list: list[str] = json.load(f)
	id_list.extend(additional_id_list)
	with open("id_list.json", "w") as f:
		json.dump(id_list, f)


def save_all_human_drugs_ids():
	last_page = get_last_page()
	for page in range(last_page + 1, 455):
		drugs = DrugsSearcher.get_human_drugs(page)
		id_list: list[str] = [drug["dragRegNum"] for drug in drugs]
		update_id_list(id_list)
		set_last_page(page)


def get_drug_info_from_api(drug_id) -> dict:
	url = "https://israeldrugs.health.gov.il/GovServiceList/IDRServer/GetSpecificDrug"
	payload = {"dragRegNum": drug_id}
	return requests.post(url, json=payload).json()


def create_list_of_drugs_info():
	with open("id_list.json", "r") as f:
		id_list = json.load(f)
	with open("info.json", "r") as f:
		drugs_information = json.load(f)
	processed_id: list[str] = [drug["Registration number"] for drug in drugs_information]
	for i, drug_id in enumerate(id_list):
		if drug_id not in processed_id:
			drugs_information.append(DrugInformationPreparer(get_drug_info_from_api(drug_id)).get_info())
			with open("info.json", "w") as f:
				json.dump(drugs_information, f, indent=4)


if __name__ == "__main__":
	create_list_of_drugs_info()
