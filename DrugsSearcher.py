
import requests


class DrugsSearcher:

	@staticmethod
	def get_all_human_drugs() -> list[dict]:
		drugs = DrugsSearcher.get_human_drugs(1)
		if len(drugs) == 10:
			for page in range(2, drugs[0]["pages"] + 1):
				drugs.extend(DrugsSearcher.get_human_drugs(page))
		return drugs

	@staticmethod
	def get_human_drugs(page) -> list[dict]:
		url = "https://israeldrugs.health.gov.il/GovServiceList/IDRServer/SearchByAdv"
		payload = {
			"cytotoxic": False,
			"fromCanceledDrags": None,
			"fromNewDrags": None,
			"fromUpdateInstructions": None,
			"healthServices": False,
			"isGSL": False,
			"isPeopleMedication": True,
			"newDragsDrop": 0,
			"orderBy": 1,
			"pageIndex": page,
			"prescription": False,
			"toCanceledDrags": None,
			"toNewDrags": None,
			"toUpdateInstructions": None,
			"types": "0",
			"val": "",
			"veterinary": False
		}
		return requests.post(url, json=payload).json()
